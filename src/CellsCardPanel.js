import { html } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import { BaseElement } from '@bbva-commons-web-components/cells-base-elements';
import styles from './CellsCardPanel-styles.js';
import '@cells-components/coronita-icons/coronita-icons.js';
import '@cells-components/cells-icon/cells-icon.js';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-card-panel></cells-card-panel>
```

##styling-doc

@customElement cells-card-panel
*/
export class CellsCardPanel extends BaseElement {
  static get is() {
    return 'cells-card-panel';
  }

  // Declare properties
  static get properties() {
    return {
      headerTitle: String,
      footer: {
        type: Boolean,
        attribute: 'footer',
      },
      collapsible: {
        type: Boolean,
        attribute: 'collapsible',
      },
      showActionButton: {
        type: Boolean,
        attribute: 'show-action-button',
      },
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.headerTitle = '';
  }

  static get styles() {
    return [styles, getComponentSharedStyles('cells-card-panel-shared-styles')];
  }

  isExpand() {
    return !this.element('.body-card').classList.contains('collapsed-card');
  }

  collapsedPanel() {
    if (!this.collapsible) {
      return;
    }
    this.element('.body-card').classList.toggle('collapsed-card');
    this.dispatch('on-collapsed-panel', {
      expand: !this.element('.body-card').classList.contains('collapsed-card'),
    });
  }

  actionButtonHandler(e) {
    e.stopPropagation();
    const { clientX, clientY } = e;
    const position = this.getPositionElement(e.target);
    this.dispatch('on-handler-action-button', { clientX, clientY, position });
  }

  // Define a template
  render() {
    return html`
      <main class="card shadow">
        <header @click="${this.collapsedPanel}" ?hidden="${!this.headerTitle}">
          <section class="title">
            <div>${this.headerTitle}</div>
          </section>
          <section class="actions">
            <slot name="top-bar-actions"></slot>
            <button
              @click="${this.actionButtonHandler}"
              ?hidden="${!this.showActionButton}"
            >
              <cells-icon icon="coronita:configuration"></cells-icon>
            </button>
          </section>
        </header>

        <section class="body-card">
          <div><slot name="toolbar-top"></slot></div>
          <div class="body-content">
            <slot name="body"></slot>
          </div>
        </section>
        <footer ?hidden="${!this.footer}">
          <slot name="footer"></slot>
        </footer>
      </main>
    `;
  }
}
