import { setDocumentCustomStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import { css } from 'lit-element';

setDocumentCustomStyles(css`
  #iframeBody {
    margin: 0 15px;
  }

  .container {
    padding: 10px;
    display: flex;
    align-items: center;
    justify-content: center;
  }

`);
